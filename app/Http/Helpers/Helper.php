<?php

use App\Models\AdminActivityLog;
use App\Models\ProfileBroadcast;
use App\Models\User;
use App\Models\UserOtp;
use App\Models\Activity;
use App\Models\ActivityLog;
use App\Models\Ethnicity;
use App\Models\Caste;
use App\Models\Sect;
use App\Models\Education;
use App\Models\Profession;
use App\Models\City;
use App\Models\Role;
use App\Models\UserLink;
use App\Models\ShareLink;
use App\Models\AppSetting;
use Google\Cloud\Translate\V2\TranslateClient;
use GuzzleHttp\Client;
use Illuminate\Support\Str;
use Lang as LA;
use App\Models\SlangWord;

if (!function_exists('getAdminRole')) {
    if (!function_exists('getAdminRole')) {
        function getAdminRole()
        {
            return "admin";
        }
    }
}

if (!function_exists('getVisitorRole')) {
    if (!function_exists('getVisitorRole')) {
        function getVisitorRole()
        {
            return "admin";
        }
    }
}


if (!function_exists('generateInvitationToken')) {
    function generateInvitationToken()
    {
        return encrypt(Str::random(32) . strtotime('now'));
    }
}
if (!function_exists('generatePIN')) {
    function generatePIN($digits = 6)
    {
        if ($digits > 0) {
            $min = (int)("1" . str_repeat("0", ($digits - 1)));
            $max = str_repeat("9", $digits);
            $pin = mt_rand($min, $max);
            return $pin;
        } else {
            return 0;
        }
    }
}
