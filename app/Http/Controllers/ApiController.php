<?php

namespace App\Http\Controllers;

use App\Models\NotificationMessage;
use App\Models\User;
use App\Models\AppVersion;
use App\Models\Notification;
use App\Models\NotificationLog;
use App\Models\UserAnalytic;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Laravel\Passport\Token;
use Laravel\Sanctum\HasApiTokens;
use Symfony\Component\HttpFoundation\Response;
use Edujugon\PushNotification\PushNotification;
use App;
use Yajra\DataTables\Exceptions\Exception;


class ApiController extends Controller
{

    public function __construct()
    {

    }

    protected function sendResponse($code = 401, $msg_key = "", $data = null)
    {


        $response["code"] = (int)$code;
        $response["message"] = (isset($msg_key)) ? $msg_key : "";
        $response["data"] = (isset($data)) ? $data : new \stdClass();
        return response()->json($response, $code)->setStatusCode($code);
    }



}
