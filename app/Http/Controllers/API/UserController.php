<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\ApiController;
use App\Mail\SendInvitation;
use App\Mail\SendSignupPin;
use App\Models\Invitation;
use App\Models\User;
use App\Models\UserOtp;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Auth;
use Validator;

class UserController extends ApiController
{

    public function __construct()
    {
        //
    }


    public function updateProfile(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required|min:4|max:20',
            'image' => 'dimensions:max_width=250,max_height=250',
        ], [
            'user_name.required' => 'User name is required',
            'user_name.unique' => 'User name already taken',
            'password.required' => 'Password is required ',
            'password.digits' => 'Invalid password entered',
        ]);
        if (!$validator->fails()) {
            $user = Auth::user();
            $user->name = $request->name;

            if ($request->hasFile('image')) {
                $filename = $request->image->getClientOriginalName();
                $request->image->storeAs('images', $filename, 'public');
                $user->image = $filename;
            }
            $user->save();
            $response['id']= $user->id;
            $response['name']= $user->name;
            $response['email']= $user->email;
            $response['user_name']= $user->user_name;
            $response['image']= $user->image;
            return $this->sendResponse(400, "User update successfully",$response);
        } else {
            return $this->sendResponse(401, $validator->errors()->first(), ["errors" => array_values($validator->errors()->toArray())]);
        }
    }


    public function verifyPin(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|exists:users,email',
            'pin' => 'required|digits:6',
        ], [
            'email.required' => 'email is required',
            'pin.required' => 'Password is required ',
            'pin.digits' => 'Invalid pin code',
        ]);
        if (!$validator->fails()) {
            $user = User::where(['email' => $request->email])->first();
            if ($user) {
                $user_otp = UserOtp::with('user')->where(['user_id' => $user->id, 'pin' => $request->pin])->first();
                if ($user_otp) {
                    $user->email_verified_at = Carbon::now();
                    $user->save();
                    $user_otp->delete();
                    $response['auth_token'] = $user->createToken('Admin')->accessToken;
                    $response['id'] = $user->id;
                    $response['name'] = $user->name;
                    return $this->sendResponse(200, "Login successfully", $response);
                }
                return $this->sendResponse(200, "Invalid pin code");
            } else {
                return $this->sendResponse(400, "Invalid information given");
            }
        } else {
            return $this->sendResponse(401, $validator->errors()->first(), ["errors" => array_values($validator->errors()->toArray())]);
        }
    }
}
