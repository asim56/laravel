<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\ApiController;
use App\Mail\SendInvitation;
use App\Mail\SendSignupPin;
use App\Models\Invitation;
use App\Models\User;
use App\Models\UserOtp;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Validator;
use Auth;

class AuthController extends ApiController
{


    public function signup(Request $request )
    {
        $validator = Validator::make($request->all(), [
            'user_name' => 'required|min:4|max:20|unique:users,user_name',
            'password' => 'required|digits:6',
        ], [
            'user_name.required' => 'User name is required',
            'user_name.unique' => 'User name already taken',
            'password.required' => 'Password is required ',
            'password.digits' => 'Invalid password entered',
        ]);
        if (!$validator->fails()) {
            $invitation_tokens = Invitation::where(['token' => $request->token])->first();
            if ($invitation_tokens) {
                $user = User::create([
                    'email' => $invitation_tokens->email,
                    'user_name' => $request->user_name,
                    'password' => bcrypt($request->password),
                ]);

                $user->assignRole(getVisitorRole());
                $pin = generatePIN();
                $user_otp = UserOtp::updateOrCreate([
                    'user_id' => $user->id,
                    'pin' => $pin,
                ]);
                Mail::to($user->email)->send(new SendSignupPin($pin));
                return $this->sendResponse(200, "Pin code for verification is sent on your email, Please verify.");
            } else {
                return $this->sendResponse(400, "Your invitation token is expired");
            }
        } else {
            return $this->sendResponse(401, $validator->errors()->first(), ["errors" => array_values($validator->errors()->toArray())]);
        }
    }


    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required|digits:6',
        ], [
            'email.required' => 'Email is required ',
            'email.email' => 'Please enter valid email id ',
            'password.required' => 'Password is required ',
            'password.digits' => 'Invalid password entered',
        ]);
        if (!$validator->fails()) {
            $user = User::role(getVisitorRole())->where(['email'=>$request->email])->first();
            if ($user && Auth::attempt(['email'=>$request->email,'password'=>$request->password])) {
                $user= $request->user();
                $response['auth_token'] = $user->createToken('Admin')->accessToken;
                $response['id'] = $user->id;
                $response['name'] = $user->name;
                return $this->sendResponse(200, "Login successfully",$response);
            } else {
                return $this->sendResponse(400, "Invalid credentials");
            }
        } else {
            return $this->sendResponse(401, $validator->errors()->first(), ["errors" => array_values($validator->errors()->toArray())]);
        }
    }

    public function verifyPin(Request $request )
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|exists:users,email',
            'pin' => 'required|digits:6',
        ], [
            'email.required' => 'email is required',
            'pin.required' => 'Password is required ',
            'pin.digits' => 'Invalid pin code',
        ]);
        if (!$validator->fails()) {
            $user =User::where(['email'=>$request->email])->first();
            if ($user) {
                $user_otp=UserOtp::with('user')->where(['user_id'=>$user->id,'pin'=>$request->pin])->first();
                if($user_otp){
                    $user->email_verified_at = Carbon::now();
                    $user->save();
                    $user_otp->delete();
                    $response['auth_token'] = $user->createToken('Admin')->accessToken;
                    $response['id'] = $user->id;
                    $response['name'] = $user->name;
                    return $this->sendResponse(200, "User Verify successfully",$response);
                }
                return $this->sendResponse(200, "Invalid pin code");
            } else {
                return $this->sendResponse(400, "Invalid information given");
            }
        } else {
            return $this->sendResponse(401, $validator->errors()->first(), ["errors" => array_values($validator->errors()->toArray())]);
        }
    }
}
