<?php

namespace App\Http\Controllers\API\Admin;

use App\Http\Controllers\ApiController;
use App\Models\User;
use Illuminate\Http\Request;
use Validator;
use Hash;
use Auth;


class AuthController extends ApiController
{
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required|digits:6',
        ], [
            'email.required' => 'Email is required ',
            'email.email' => 'Please enter valid email id ',
            'password.required' => 'Password is required ',
            'password.digits' => 'Invalid password entered',
        ]);
        if (!$validator->fails()) {
            try {
                $user = User::role(getAdminRole())->where(['email' => $request->email])->first();
                if ($user && Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
                    $user = $request->user();
                    $response['auth_token'] = $user->createToken('admin')->accessToken;
                    $response['id'] = $user->id;
                    $response['name'] = $user->name;
                    return $this->sendResponse(200, "Login successfully", $response);
                } else {
                    return $this->sendResponse(400, "Invalid credentials");
                }
            }catch (\Exception $e){
                return $this->sendResponse(400, $e->getTrace());
            }
        } else {
            return $this->sendResponse(401, $validator->errors()->first(), ["errors" => array_values($validator->errors()->toArray())]);
        }
    }
}
