<?php

namespace App\Http\Controllers\API\Admin;

use App\Http\Controllers\ApiController;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendInvitation;
use App\Models\Invitation;
use Illuminate\Http\Request;
use Validator;
use Hash;
use Auth;


class InvitationController extends ApiController
{
    public function sendInvitation(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
        ], [
            'email.required' => 'Email is required ',
            'email.email' => 'Please enter valid email id ',
        ]);
        if (!$validator->fails()) {
            try {
                $invitation = Invitation::create([
                    'email' => $request->email,
                    'token' => generateInvitationToken(),
                ]);
                $signup_link = URL('api/user/signup').'?token='.$invitation->token;
                Mail::to($request->email)->send(new SendInvitation($signup_link));
                return $this->sendResponse(200, "Invitation sent successfully");
            } catch (\Exception $e) {
                return $this->sendResponse(200, $e->getMessage());
            }
        } else {
            return $this->sendResponse(401, $validator->errors()->first(), ["errors" => array_values($validator->errors()->toArray())]);
        }
    }
}
