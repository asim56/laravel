<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendInvitation extends Mailable
{
    use Queueable, SerializesModels;


    protected $signup_link;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($signup_link)
    {
        $this->signup_link=$signup_link;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.send_invitation', [
            'signup_link' => $this->signup_link
        ]);
    }
}
