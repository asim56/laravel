<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Spatie\Permission\Models\Role;
use DB;

class CreateAdminUser extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = Role::create(['name' => 'admin']);
        $role = Role::create(['name' => 'visitor']);
        DB::table('users')->insert([
            [
                'id'=>1,
                'name'=>'Admin',
                'user_name'=>'admin',
                'email'=>'admin@mail.com',
                'password'=>'$2y$10$vjxhlI.KF5U6fB7pri8A3O67MIBkB7pxDav7XSEtoHFwZ9BaXc.p.',
                'email_verified_at'=>Carbon::now(),
                'created_at' => Carbon::now(), "updated_at" => Carbon::now()
            ]
        ]);

        DB::table('model_has_roles')->insert([
            ['role_id' => 1, 'model_type' => 'App\Models\User', 'model_id' => 1],
        ]);
    }
}
