<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



Route::post('admin/login', [\App\Http\Controllers\API\Admin\AuthController::class, 'login']);

Route::post('user/signup/{token?}', [\App\Http\Controllers\API\AuthController::class, 'signup']);
Route::post('user/verify_pin', [\App\Http\Controllers\API\AuthController::class, 'verifyPin']);
Route::post('user/login', [\App\Http\Controllers\API\AuthController::class, 'login']);

Route::group(['middleware' => 'auth:api'], function () {

    Route::group(['middleware' => ['role:admin'], 'prefix' => 'admin'], function () {
        Route::post('send_invitation', [\App\Http\Controllers\API\Admin\InvitationController::class, 'sendInvitation']);
    });

    Route::group(['prefix' => 'user'], function () {
        Route::post('update_profile', [\App\Http\Controllers\API\UserController::class, 'updateProfile']);

    });


});
