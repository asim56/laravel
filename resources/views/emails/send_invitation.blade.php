@component('mail::message')
Hi,

SoloTach send you signup invitation, For signup please send your username and password on below link in POST request.
{{$signup_link}}


Thanks,<br>
{{ config('app.name') }}
@endcomponent
