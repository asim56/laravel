@component('mail::message')
Hi,

Your verification pin is : {{$pin}}

Thanks,<br>
{{ config('app.name') }}
@endcomponent
